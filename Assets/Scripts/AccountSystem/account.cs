using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace WebAuthAPI
{
    
[System.Serializable]
public class AccountJSON
{
    public string phonenum;
    public string rolename;
    public uint   coins;
    public uint   diamonds;
    public bool   forbidden;
    
    public static AccountJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<AccountJSON>(json);
    }

    public string toJSON()
    {
        return JsonUtility.ToJson(this);
    }
};

[System.Serializable]
public class GetAccountInfoJSON
{
    public int code;
    public AccountJSON info;
    
    public static GetAccountInfoJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<GetAccountInfoJSON>(json);
    }
};

[System.Serializable]
public class UpdateRoleNameJSON
{
    public int code;
    
    public static UpdateRoleNameJSON fromJSON(string json)
    {
        return JsonUtility.FromJson<UpdateRoleNameJSON>(json);
    }
};

public class Account
{
    Authenticator authenticator;

    public const string GET_ACCOUNT_INFO_URL = Authenticator.ACCOUNT_SERVER_URL + "/getaccountinfo";
    public const string UPDATE_ROLE_NAME_URL = Authenticator.ACCOUNT_SERVER_URL + "/updaterolename";
    
    public Account(Authenticator authenticator)
    {
        this.authenticator = authenticator;
    }

    public void getAccountInfo(Action<int, object> callback)
    {
        if (!this.authenticator.isLoaded()) {
            callback(ServErr.SERV_ERR_AUTHENTICATOR_NOT_LOADED, null);
        }
        else {
            this.authenticator.sessionVerify(this.getAccountInfoPostForm, null, callback);
        }
    }

    public void updateRoleName(string rolename, Action<int, object> callback)
    {
        if (!this.authenticator.isLoaded()) {
            callback(ServErr.SERV_ERR_AUTHENTICATOR_NOT_LOADED, null);
        }
        else {
            this.authenticator.sessionVerify(this.updateRoleNamePostForm, rolename, callback);
        }
    }
    
    private IEnumerator getAccountInfoPostForm(string phonenum, string idortoken, object passby, Action<int, object> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", phonenum);
        form.AddField("idortoken", idortoken);
        UnityWebRequest request = UnityWebRequest.Post(GET_ACCOUNT_INFO_URL, form);
			request.chunkedTransfer = false;
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {
                Debug.Log("post to " + GET_ACCOUNT_INFO_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_POST_GET_ACCOUNT_INFO_FORM, null);
        }
        else {
            var result = GetAccountInfoJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {
                    Debug.Log("get account info failed with: " + result.code + "!");
                }
                callback(result.code, null);
            }
            else {
                if (Debug.isDebugBuild) {                
                    Debug.Log("get account info successful!");
                }
                callback(result.code, result.info);
            }
        }
    }

    private IEnumerator updateRoleNamePostForm(string phonenum, string idortoken, object passby, Action<int, object> callback)
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", phonenum);
        form.AddField("idortoken", idortoken);
        form.AddField("rolename", (string)passby);
        UnityWebRequest request = UnityWebRequest.Post(UPDATE_ROLE_NAME_URL, form);
			request.chunkedTransfer = false;
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError) {
            if (Debug.isDebugBuild) {
                Debug.Log("post to " + UPDATE_ROLE_NAME_URL + " failed with: " + request.error);
            }
            callback(ServErr.SERV_ERR_POST_UPDATE_ROLE_NAME_FORM, null);
        }
        else {
            var result = UpdateRoleNameJSON.fromJSON(request.downloadHandler.text);
            if (result.code != ServErr.SERV_ERR_SUCCESSFUL) {
                if (Debug.isDebugBuild) {
                    Debug.Log("update role name failed with: " + result.code + "!");
                }
                callback(result.code, null);
            }
            else {
                if (Debug.isDebugBuild) {                
                    Debug.Log("update role name successful!");
                }
                callback(result.code, null);
            }
        }
    }    
};

};


