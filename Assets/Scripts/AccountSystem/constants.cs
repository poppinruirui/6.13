public class Constants {
    public const string LoadingLocaleResources = "本地资源加载中 ...";
    public const string LocaleResourceLoaded = "本地资源加载完成";
    public const string LocaleResourceLoadFailed = "无法更新本地资源, 请检查网络是否可用!";
    public const string LoadingRemoteResources = "远程资源加载中 ...";
    public const string RemoteResourceLoaded = "远程资源加载完成";
    public const string RemoteResourceLoadFailed = "无法更新远程资源, 请检查网络是否可用!";
};
