﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class CBlackAndWhiteShader : MonoBehaviour
{
    public static CBlackAndWhiteShader s_Instance;

    public Shader curShader;
    [Range(0.0f, 1.0f)]
    public float grayScaleAmount = 1.0f;
    private Material curMaterial;

    float m_fFadeTotalTime = 0f;
    bool m_bFading = false;

    public Material CurMaterial
    {
        get
        {
            if (curMaterial == null)
            {
                Material mat = new Material(curShader);
                mat.hideFlags = HideFlags.HideAndDontSave;
                curMaterial = mat;
            }
            return curMaterial;
        }
    }

    private void Awake()
    {
        s_Instance = this;
    }

    private void Update()
    {
        Fading();
    }

    // Use this for initialization
    void Start()
    {
        if (!SystemInfo.supportsImageEffects)
        {
            enabled = false;
            return;
        }
        if (!curShader && !curShader.isSupported)
        {
            enabled = false;
        }
    }

    void OnDisable()
    {
        if (curMaterial)
        {
            DestroyImmediate(curMaterial);
        }
    }

    void OnRenderImage(RenderTexture srcTexture, RenderTexture destTexture)
    {
        if (curShader != null)
        {
            CurMaterial.SetFloat("_LuminosityAmount", grayScaleAmount);
            Graphics.Blit(srcTexture, destTexture, CurMaterial);
        }
        else
        {
            Graphics.Blit(srcTexture, destTexture);
        }
    }

    public void BeginFade(float fFadeTotalTime)
    {
        m_fFadeTotalTime = fFadeTotalTime;
        grayScaleAmount = 0.0f;
        m_bFading = true;
    }

    void Fading()
    {
        if (!m_bFading)
        {
            return;
        }

        grayScaleAmount += Time.deltaTime;
        if (grayScaleAmount >= 1f)
        {
            grayScaleAmount = 1f;
            EndFade();
        }
    }

    void EndFade()
    {
        m_bFading = false;
    }

    public void RecoverColor()
    {
        grayScaleAmount = 0f;
    }

}