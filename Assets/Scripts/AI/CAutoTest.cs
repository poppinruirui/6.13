﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CAutoTest : MonoBehaviour {

    public static CAutoTest s_Instance;

    Player m_Player;
    bool m_bAutoTesing = false;
    static Vector3 vecTempPos = new Vector3();
    static Vector2 vecTempDir = new Vector2();

    Vector3 m_vecWorldCursorPos = new Vector3( 1000f, 0f, 0f );

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}

    public bool IsAutoTesting()
    {
        return m_bAutoTesing;
    }
	
	// Update is called once per frame
	void Update () {
		if ( !m_bAutoTesing)
        {
            return;
        }

        AutoMove();
        AutoAddVolume();
        AutoSpitBall();
        ForceSpitLoop();
    }

    public void BeginPlayerAutoTest( Player player )
    {
        m_Player = player;
        m_bAutoTesing = true;
        ChangeMoveDir();
        Main.s_Instance.m_MainPlayer.SetMoving(true);
        CSkillSystem.s_Instance.SetTotalPoint(100);
        CSkillSystem.s_Instance.AddPoint(1, true);
        CSkillSystem.s_Instance.AddPoint(2, true);
    }

    void ChangeMoveDir()
    {
        m_vecWorldCursorPos.x = (float)UnityEngine.Random.Range(MapEditor.s_Instance.GetWorldLeft(), MapEditor.s_Instance.GetWorldRight());
        m_vecWorldCursorPos.y = (float)UnityEngine.Random.Range(MapEditor.s_Instance.GetWorldBottom(), MapEditor.s_Instance.GetWorldTop());
        m_vecWorldCursorPos.Normalize();
        m_vecWorldCursorPos *= MapEditor.s_Instance.GetWorldSize();
    }

    float m_fChangeMoveDirInterval = 0f;
    void AutoMove()
    {
        if ( PlayerAction.s_Instance.balls_min_position.x <= MapEditor.s_Instance.GetWorldLeft() + 50f )
        {
            if (m_vecWorldCursorPos.x < 0f)
            {
                m_vecWorldCursorPos.x = -m_vecWorldCursorPos.x;
            }
        }

        if (PlayerAction.s_Instance.balls_max_position.x >= MapEditor.s_Instance.GetWorldRight() - 50f)
        {
            if (m_vecWorldCursorPos.x > 0f)
            {
                m_vecWorldCursorPos.x = -m_vecWorldCursorPos.x;
            }
        }

        if (PlayerAction.s_Instance.balls_min_position.y <= MapEditor.s_Instance.GetWorldBottom() + 50f)
        {
            if (m_vecWorldCursorPos.y < 0f)
            {
                m_vecWorldCursorPos.y = -m_vecWorldCursorPos.y;
            }
        }

        if (PlayerAction.s_Instance.balls_max_position.y >= MapEditor.s_Instance.GetWorldTop() - 50f)
        {
            if (m_vecWorldCursorPos.y > 0f)
            {
                m_vecWorldCursorPos.y = -m_vecWorldCursorPos.y;
            }
        }

        m_fChangeMoveDirInterval += Time.deltaTime;
        if (m_fChangeMoveDirInterval >= 20f)
        {
            ChangeMoveDir();
            m_fChangeMoveDirInterval = 0f;
        }
    }

    float m_fAddVolumeInterval = 0f;
    void AutoAddVolume()
    {
        m_fAddVolumeInterval += Time.deltaTime;
        if (m_fAddVolumeInterval < 5f)
        {
            return;
        }
        m_fAddVolumeInterval = 0f;

        if ( m_Player.GetTotalVolume() < 3000f )
        {
            m_Player.Test_AddAllBallSize(500);
        }
    }

    float m_fSpitBallInterval = 0f;
    void AutoSpitBall()
    {
        m_fSpitBallInterval += Time.deltaTime;
        if (m_fSpitBallInterval < 5f)
        {
            return;
        }
        m_fSpitBallInterval = 0f;
        if ( m_Player.GetCurLiveBallNum() < Main.s_Instance.m_fMaxBallNumPerPlayer )
        {
            /*
            m_Player.SetSpitPercent( 0.5f );
            m_Player.SetSpecialDirection(vecTempDir);
            m_Player.SetSpitParams( 4f, 60f, 10f );
            vecTempDir.x = 0.5f;
            vecTempDir.y = 0.5f;
            */
            if (m_bForceSpiting == false)
            {
                BeginForceSpit();
            }
        }
    }

    bool m_bForceSpiting = false;
    void BeginForceSpit()
    {
        m_bForceSpiting = true;
        Main.s_Instance.BeginSpit();
    }

    float m_fForceSpitingTimeElapse = 0f;
    void ForceSpitLoop()
    {
        if ( !m_bForceSpiting)
        {
            return;
        }

        m_fForceSpitingTimeElapse += Time.deltaTime;
        if (m_fForceSpitingTimeElapse >= 2f)
        {
            m_fForceSpitingTimeElapse = 0;
            m_bForceSpiting = false;
        }
    }


    public float GetPlayerMoveSpeed()
    {
        return 50f;
    }

    public Vector3 GetWorldCursorPosition()
    {
        return m_vecWorldCursorPos;
    }
}
