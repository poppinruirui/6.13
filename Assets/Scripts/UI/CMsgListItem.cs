﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CMsgListItem : MonoBehaviour {

    public Text _txt1;
    public Text _txt2;
    public Image img;
    public Image imgHighLight;
    public Image _imgKiller;
    public Image _imgDeadMeat;

    float m_fAlpha = 1.0f;
    bool m_bFading = false;

    static Color cTempColor = new Color();
    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();
    static Rect tempRect = new Rect();

    List<Image> m_lstAssistAvatar = new List<Image>();

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Fading();

    }

    public void  SetText1( string szContent )
    {
        _txt1.text = szContent;
    }
    public void SetText2(string szContent)
    {
        _txt2.text = szContent;
    }

    public void SetKillerAvatar(Sprite sprKillerAvatar)
    {
        _imgKiller.sprite = sprKillerAvatar;
    }

    public void SetAssistAvatar(List<Sprite> lstAssistAvatar )
    { 
        for ( int i = 0; i < lstAssistAvatar.Count; i++ )
        {
            Image imgAssist = null;
            if (i >= m_lstAssistAvatar.Count)
            {
                imgAssist = GameObject.Instantiate(CMsgList.s_Instance.m_preAssistAvatar) as Image;
                imgAssist.transform.SetParent(this.transform);
                RectTransform rtKiller = _imgKiller.GetComponent<RectTransform>();
                RectTransform rtAssist = imgAssist.GetComponent<RectTransform>();
                vecTempScale.x = 0.7f;
                vecTempScale.y = 0.7f;
                vecTempScale.z = 1f;
                rtAssist.localScale = vecTempScale;
                float fAssistWidth = rtKiller.rect.width * vecTempScale.x;
                if ( i == 0)
                {
                    vecTempPos.x = _imgKiller.transform.localPosition.x - (rtKiller.rect.width + fAssistWidth) / 2.0f;
                }
                else
                {
                    Image imgPreAssist = m_lstAssistAvatar[i - 1];
                    RectTransform rtPreAssist = imgPreAssist.GetComponent<RectTransform>();
                    vecTempPos.x = imgPreAssist.transform.localPosition.x - fAssistWidth;
                }
                vecTempPos.y = _imgKiller.transform.localPosition.y;
                rtAssist.localPosition = vecTempPos;


                m_lstAssistAvatar.Add(imgAssist);
            }
            else
            {
                imgAssist = m_lstAssistAvatar[i];
                imgAssist.gameObject.SetActive(true);
            }
            imgAssist.sprite = lstAssistAvatar[i];

        }
    }

    public void SetDeadMeatAvatar( Sprite sprDeadMeat )
    {
        _imgDeadMeat.sprite = sprDeadMeat;
    }

    public void BeginFade()
    {
        m_bFading = true;
    }

    void Fading()
    {
        if ( !m_bFading)
        {
            return;
        }

        m_fAlpha -= Time.deltaTime;

        cTempColor = _txt1.color;
        cTempColor.a = m_fAlpha;
        _txt1.color = cTempColor;

        cTempColor = _txt2.color;
        cTempColor.a = m_fAlpha;
        _txt2.color = cTempColor;


        cTempColor = img.color;
        cTempColor.a = m_fAlpha;
        img.color = cTempColor;

        cTempColor = imgHighLight.color;
        cTempColor.a = m_fAlpha;
        imgHighLight.color = cTempColor;

    }

    public bool FadeCompleted()
    {
        return m_fAlpha <= 0f;
    }

    public void ReInit()
    {
        m_fAlpha = 1.0f;

        cTempColor = _txt1.color;
        cTempColor.a = 1f;
        _txt1.color = cTempColor;

        cTempColor = _txt2.color;
        cTempColor.a = 1f;
        _txt2.color = cTempColor;


        cTempColor = img.color;
        cTempColor.a = 1f;
        img.color = cTempColor;

        cTempColor = imgHighLight.color;
        cTempColor.a = 1f;
        imgHighLight.color = cTempColor;

        m_bFading = false;

        for ( int  i = 0; i < m_lstAssistAvatar.Count; i++ )
        {
            m_lstAssistAvatar[i].gameObject.SetActive( false );
        }
     }
}
