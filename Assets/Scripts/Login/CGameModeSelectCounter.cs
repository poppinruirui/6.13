﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CGameModeSelectCounter : MonoBehaviour {

    public GameObject _goSelected;

    static Vector3 vecTempScale = new Vector3();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetSelected( bool bSelectced )
    {
        _goSelected.SetActive(bSelectced);

        if (bSelectced)
        {
            vecTempScale.x = 1.276f;
            vecTempScale.y = 1.276f;
            vecTempScale.z = 1.0f;
        }
        else
        {
            vecTempScale.x = 1.16f;
            vecTempScale.y = 1.16f;
            vecTempScale.z = 1f;
        }
        this.transform.localScale = vecTempScale;


    }
}
