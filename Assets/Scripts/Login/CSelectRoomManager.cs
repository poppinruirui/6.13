﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CSelectRoomManager : MonoBehaviour
{
    public static CSelectRoomManager s_Instance = null;

    public const int MAX_PLAYER_COUNT_PER_ROOM = 10;

    public GameObject _panelSelectRoom;
    public GameObject[] m_arySelectRoomPanel;


    public CCommonJinDuTiao _jindutiao;

    public Toggle _toggleEnterTestRoom;
    public Toggle _toggleReadFromLocal;

    public CRoom[] m_aryRooms;
    public CRoom[] m_aryRooms_Others;
    public Sprite[] m_aryRoomAvatar;

    public Sprite m_sprNotSelected;
    public Sprite m_sprSelected;



    /// <summary>
    /// UI
    /// </summary>
    public Text _txtCurSelectedRoomName;
    public Text[] m_arySelectedRoomName;

    public enum eCtrlType
    {
        select_room_panel,
        cur_selected_room_name,
    };

    public UnityEngine.Object GetCtrl(eCtrlType eType)
    {
        int nDeviceType = (int)CAdaptiveManager.s_Instance.GetCurDeviceType();

        switch (eType)
        {
            case eCtrlType.select_room_panel:
                {
                    return m_arySelectRoomPanel[nDeviceType];
                }
                break;
            case eCtrlType.cur_selected_room_name:
                {
                    return m_arySelectedRoomName[nDeviceType];
                }
                break;
        }

        return null;
    }

    public CRoom[] GetRooms()
    {
        CAdaptiveManager.eDeviceTYpe eDeviceType = CAdaptiveManager.s_Instance.GetCurDeviceType();
        if (eDeviceType == CAdaptiveManager.eDeviceTYpe.iPhone_X)
        {
            return m_aryRooms;
        }
        else
        {
            return m_aryRooms_Others;
        }
    }

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		CSelectSkill.s_Instance.LoadMap ("");

        CRoom[] aryRooms = GetRooms();

        // 初始化房间
        for ( int i = 0; i < aryRooms/*m_aryRooms*/.Length; i++ )
        {
            CRoom room = aryRooms/*m_aryRooms*/[i];
            room.SetAvatar(m_aryRoomAvatar[i]);
            room.SetRoomName( "测试房间" + (i+1) );
            room.SetRoomIndex( i );
        }

       
    }

    CRoom m_CurSelectedRoom = null;
    public void SelectRoom( CRoom room )
    {
        if (m_CurSelectedRoom != null)
        {
            m_CurSelectedRoom.SetSelected( false );
        }
        m_CurSelectedRoom = room;
        m_CurSelectedRoom.SetSelected(true);
        (GetCtrl( eCtrlType.cur_selected_room_name ) as Text)/*_txtCurSelectedRoomName*/.text = m_CurSelectedRoom.GetRoomName();
    }

    Dictionary<int, int> m_dicCurRoomInfo = new Dictionary<int, int>();

        // Update is called once per frame
    void Update () {

        if ( !PhotonNetwork.connected )
        {
            return;
        }

        RoomInfo[] aryRoomInfo = PhotonNetwork.GetRoomList();
        m_dicCurRoomInfo.Clear();
        for ( int i = 0; i < aryRoomInfo.Length; i++ )
        {
            int nRoomId = 0;
            if ( !int.TryParse(aryRoomInfo[i].Name, out nRoomId) )
            {
                continue;
            }
            m_dicCurRoomInfo[nRoomId] = aryRoomInfo[i].PlayerCount;
        }

        CRoom[] aryRooms = GetRooms();
        for (int i = 0; i < aryRooms.Length; i++)
        {
            CRoom room = aryRooms[i];
            int nCurPlayerCount = 0;
            if ( !m_dicCurRoomInfo.TryGetValue( i, out nCurPlayerCount) )
            {
                nCurPlayerCount = 0;
            }
            room.SetPlayerCount(nCurPlayerCount, MAX_PLAYER_COUNT_PER_ROOM);
        }
    }

    public void EnterRoom( int nRoomIndex )
    {
        AccountManager.s_Instance.PlaySelectedRoom_Test(nRoomIndex);
        ShowJinDuTiao();
    }

    public void OnClickButton_0()
    {
        AccountManager.s_Instance.PlaySelectedRoom_Test( 0 );
        ShowJinDuTiao();
    }

    public void OnClickButton_1()
    {
        AccountManager.s_Instance.PlaySelectedRoom_Test(1);
        ShowJinDuTiao();
    }


    public void OnClickButton_2()
    {
        AccountManager.s_Instance.PlaySelectedRoom_Test(2);
        ShowJinDuTiao();
    }

    public void ShowJinDuTiao()
    {
        _jindutiao.gameObject.SetActive( true );
        _jindutiao.SetInfo( "正在登入游戏" );
    }

    public void OnClickButton_PrevPage()
    {
        SetSelectRoomPanelVisible(false);

        (CAccountSystem.s_Instance.GetCtrl(CAccountSystem.eCtrlType.home_page_panel) as GameObject).SetActive(true);
    }

    public void OnClickButton_EnterSelectRoomPanel()
    {
        SetSelectRoomPanelVisible( true );

        (CAccountSystem.s_Instance.GetCtrl(CAccountSystem.eCtrlType.select_skill_panel) as GameObject).SetActive(false);
    }

    public void SetSelectRoomPanelVisible( bool bVisible )
    {
        (GetCtrl( eCtrlType.select_room_panel ) as GameObject)/*_panelSelectRoom*/.SetActive(bVisible);
       // (CAccountSystem.s_Instance.GetCtrl(CAccountSystem.eCtrlType.select_skill_panel) as GameObject).SetActive(!bVisible);
    }

    public static bool s_bEnterTestRoom = false;
    public static bool s_bReadFromLocal = false;
    public void OnToggleValueChanged_EnterTestRoom()
    {
        s_bEnterTestRoom = _toggleEnterTestRoom.isOn;
		CSelectSkill.s_Instance.LoadMap ("");
    }

    public void OnToggleValueChanged_ReadFromLocal()
    {
        s_bReadFromLocal = _toggleReadFromLocal.isOn;
    }

}
